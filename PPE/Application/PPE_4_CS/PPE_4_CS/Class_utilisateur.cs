﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace PPE_4_CS
{
    class Class_utilisateur
    {
        private static string _login;
        private static int _id;
        private static string _nom;
        private static string _prenom;
        private static int _count;
        private static int _voeu_1;
        private static int _voeu_2;
        private static int _voeu_3;
        private static string _reg_1;
        private static string _reg_2;
        private static string _reg_3;
        private static string _ordre;
        private static string _mod_voeu_1;
        private static string _mod_voeu_2;
        private static string _mod_voeu_3;
        private static bool _panel;
        private static int _acceptation;
        private string _statut;

        public static string Login
        {
            get { return _login; }
            set { _login = value; }
        }

        public static int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public static string Nom
        {
            get { return _nom; }
            set { _nom = value; }
        }

        public static string Prenom
        {
            get { return _prenom; }
            set { _prenom = value; }
        }

        public static int Count
        {
            get { return _count; }
            set { _count = value; }
        }

        public static int Voeu_1
        {
            get { return _voeu_1; }
            set { _voeu_1 = value; }
        }

        public static int Voeu_2
        {
            get { return _voeu_2; }
            set { _voeu_2 = value; }
        }

        public static int Voeu_3
        {
            get { return _voeu_3; }
            set { _voeu_3 = value; }
        }

        public static string Reg_1
        {
            get { return _reg_1; }
            set { _reg_1 = value; }
        }

        public static string Reg_2
        {
            get { return _reg_2; }
            set { _reg_2 = value; }
        }

        public static string Reg_3
        {
            get { return _reg_3; }
            set { _reg_3 = value; }
        }

        public static string Ordre
        {
            get { return _ordre; }
            set { _ordre = value; }
        }

        public static string Mod_voeu_1
        {
            get { return _mod_voeu_1; }
            set { _mod_voeu_1 = value; }
        }

        public static string Mod_voeu_2
        {
            get { return _mod_voeu_2; }
            set { _mod_voeu_2 = value; }
        }

        public static string Mod_voeu_3
        {
            get { return _mod_voeu_3; }
            set { _mod_voeu_3 = value; }
        }

        public static bool Panel
        {
            get { return _panel; }
            set { _panel = value; }
        }

        public static int Acceptation
        {
            get { return _acceptation; }
            set { _acceptation = value; }
        }

        public string Statut
        {
            get { return _statut; }
            set { _statut = value; }
        }

        public static void Procedure_CheckBox(CheckBox ckbx, Button btn, ComboBox cbbx)
        {
           

           if(ckbx.Checked == true)
            {
                Count += 1;
                cbbx.Enabled = true;                              
            }
           else
            {
                Count -= 1;
                cbbx.Enabled = false;
            }

            if (Count > 3)
            {
                btn.Enabled = false;
                
                MessageBox.Show("Vous ne pouvez faire que 3 voeux maximum !","Attention",MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                btn.Enabled = true;                
            }
        }

        public static void Procedure_Enabled(TableLayoutPanel tlp)
        {
            foreach(ComboBox cbx in tlp.Controls.OfType<ComboBox>())
            {
                Voeu_1 = 1;
                Voeu_2 = 2;
                Voeu_3 = 3;

                cbx.Enabled = false;
                cbx.DropDownStyle = ComboBoxStyle.DropDownList;
                cbx.Cursor = Cursors.Hand;
                cbx.Items.Add(Voeu_1);
                cbx.Items.Add(Voeu_2);
                cbx.Items.Add(Voeu_3);
            }

            foreach(CheckBox cbx in tlp.Controls.OfType<CheckBox>())
            {
                cbx.Cursor = Cursors.Hand;
            }
            
        }

        public static void Procedure_Enabled_checkbox(TableLayoutPanel tlp, bool booleen)
        {
            if(booleen == true)
            {
                foreach (CheckBox ckbx in tlp.Controls.OfType<CheckBox>())
                {
                    ckbx.Enabled = false;
                }
            }
            else
            {
                foreach (CheckBox ckbx in tlp.Controls.OfType<CheckBox>())
                {
                    ckbx.Enabled = true;
                }
            }            
        }


        public static void Procedure_ordre(ComboBox cbbx, Label reg1, Label reg2, Label reg3)
        {
            try
            {
                ClassConnexion obj_co = new ClassConnexion();

                obj_co.Connexion();

                if (Fonction_ordre(cbbx) == 1)
                {
                    reg1.Text = obj_co.Select_text("SELECT `nom` FROM `region` WHERE `id_Region` = '" + cbbx.Tag + "'");
                }
                else if (Fonction_ordre(cbbx) == 2)
                {
                    reg2.Text = obj_co.Select_text("SELECT `nom` FROM `region` WHERE `id_Region` = '" + cbbx.Tag + "'");
                }
                else if (Fonction_ordre(cbbx) == 3)
                {
                    reg3.Text = obj_co.Select_text("SELECT `nom` FROM `region` WHERE `id_Region` = '" + cbbx.Tag + "'");
                }

                obj_co.Deconnexion();
            }
            catch (Exception ex)
            {
                MessageBox.Show("L'erreur : " + ex + " est survenue.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        public static int Fonction_ordre(ComboBox cbbx)
        {
            return Convert.ToInt32(cbbx.SelectedItem);
        }

        public static void TextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsControl(e.KeyChar) || !Char.IsNumber(e.KeyChar))
            {
                e.Handled = true; // Set l'evenement comme etant completement fini
                return;
            }
        }

        public static void Modif_place(TextBox tbx,Label lbl)
        {
            try
            {
                string requete = "UPDATE `region` SET `nbPlace` = '" + tbx.Text + "' WHERE `region`.`id_Region` = " + tbx.Tag + " ";


                ClassConnexion obj_co = new ClassConnexion();

                obj_co.Connexion();

                string reg = obj_co.Select_text("SELECT `nom` FROM `region` WHERE `id_Region` = " + tbx.Tag + "");

                if (tbx.Text != null)
                {
                    int number;

                    bool success = Int32.TryParse(tbx.Text, out number);
                    if (success)
                    {
                        if (tbx.Text != lbl.Text)
                        {
                            DialogResult rep;
                            rep = MessageBox.Show("Voulez vous modifier le nombre de places de la région " + reg + " par : " + tbx.Text + " ?", "Information", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

                            if (rep == DialogResult.OK)
                            {
                                obj_co.Execute_requete(requete);
                                MessageBox.Show("Demande réussie.\nLa région " + reg + " possède maintenant " + tbx.Text + " place(s).", "Information", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                                lbl.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = " + tbx.Tag + "");
                                tbx.Text = "";
                            }
                        }
                        else
                        {
                            MessageBox.Show("Le nombre de places demandé est identique au nombre de place actuel", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }

                    }
                    else
                    {
                        MessageBox.Show("Le nombre de places demandé n'est pas valide !", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }

                }
                else
                {
                    MessageBox.Show("Le nombre de places demandé n'est pas valide !", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                obj_co.Deconnexion();
            }
            catch (Exception ex)
            {
                MessageBox.Show("L'erreur : " + ex + " est survenue.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            

        }

        

    }
}
