﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;



namespace PPE_4_CS
{
    public partial class AffectationVoeux : Form
    {
        ClassConnexion obj_co = new ClassConnexion();

        public AffectationVoeux()
        {
            InitializeComponent();
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            try
            {
                obj_co.Connexion();

                Class_utilisateur.Procedure_Enabled(tableLayoutPanel1);

                MySqlDataReader mon_reader;

                string nouveau_voeu = "SELECT DISTINCT `prenom`, `nom` FROM `utilisateur` JOIN `voeux` ON `utilisateur`.`id_utilisateur` = `voeux`.`id_utilisateur`  WHERE `etat` = 'Nouveau'";

                try
                {
                    mon_reader = obj_co.combo_general(nouveau_voeu);

                    while (mon_reader.Read())
                    {
                        comboBox1.Items.Add(mon_reader[0].ToString() + " " + mon_reader[1].ToString());
                    }
                    mon_reader.Close();

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Une erreur est survenue : " + ex, "Erreur");
                }

                //Ecrit le nombre de places disponibles pour chaque région
                label_ARA.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 1");
                label_BFC.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 2");
                label_Bre.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 3");
                label_CVL.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 4");
                label_Cor.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 5");
                label_GrE.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 6");
                label_HdF.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 7");
                label_IdF.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 8");
                label_Nor.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 9");
                label_NAq.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 10");
                label_Occ.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 11");
                label_PdL.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 12");
                label_PACA.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 13");
                label_Gua.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 14");
                label_Guy.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 15");
                label_Mar.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 16");
                label_Reu.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 17");
                label_May.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 18");

                comboBox2.Items.Add("Date");
                comboBox2.Items.Add("Etat");
                comboBox2.Items.Add("Région");
                comboBox3.Enabled = false;
                dateTimePicker1.Enabled = false;

                foreach (Button btn in tableLayoutPanel1.Controls.OfType<Button>())
                {
                    btn.Cursor = Cursors.Hand;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Une erreur est survenue : " + ex, "Erreur");
            }
            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string nom_prenom = comboBox1.Text;
                char[] split_p_n = new char[] { ' ' };
                string[] prenom = nom_prenom.Split(split_p_n);
                Class_utilisateur.Prenom = prenom[0];
                Class_utilisateur.Nom = prenom[1];

                label10.Text = obj_co.Select_text("SELECT `nom` FROM `region` JOIN `voeux` ON region.id_Region = voeux.id_Region_CHOISIR  WHERE `id_utilisateur` = " + obj_co.Select_text("SELECT `id_utilisateur` FROM `utilisateur` WHERE `prenom` = '" + Class_utilisateur.Prenom + "' AND `nom` = '" + Class_utilisateur.Nom + "'") + " AND `Ordre` = 1 ");
                label1.Text = obj_co.Select_text("SELECT `nom` FROM `region` JOIN `voeux` ON region.id_Region = voeux.id_Region_CHOISIR  WHERE `id_utilisateur` = " + obj_co.Select_text("SELECT `id_utilisateur` FROM `utilisateur` WHERE `prenom` = '" + Class_utilisateur.Prenom + "' AND `nom` = '" + Class_utilisateur.Nom + "'") + " AND `Ordre` = 2 ");
                label12.Text = obj_co.Select_text("SELECT `nom` FROM `region` JOIN `voeux` ON region.id_Region = voeux.id_Region_CHOISIR  WHERE `id_utilisateur` = " + obj_co.Select_text("SELECT `id_utilisateur` FROM `utilisateur` WHERE `prenom` = '" + Class_utilisateur.Prenom + "' AND `nom` = '" + Class_utilisateur.Nom + "'") + " AND `Ordre` = 3 ");

                int point = Convert.ToInt32(obj_co.Select_text("SELECT `date_entree` FROM `utilisateur` WHERE `prenom` = '" + prenom[0] + "' AND `nom`  = '" + prenom[1] + "'"));
                int date = Convert.ToInt32(DateTime.Now.ToString("yyyy"));
                int total = (date - point) * 6;

                label_v_prenom.Text = prenom[0];
                label_v_nom.Text = prenom[1];
                label_v_point.Text = Convert.ToString(total);

                btn_accepter.Enabled = true;
                btn_accepter_2.Enabled = true;
                btn_accepter_3.Enabled = true;
                btn_refuser.Enabled = true;
                btn_refuser_2.Enabled = true;
                btn_refuser_3.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Une erreur est survenue : " + ex, "Erreur");
            }

        }

        private void btn_deconnexion_Click(object sender, EventArgs e)
        {
            try
            {
                FormConnexion form_1 = new FormConnexion();
                this.Close();
                form_1.Show();
                obj_co.Deconnexion();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Une erreur est survenue : " + ex, "Erreur");
            }
            
        }

        private void btn_quiter_Click(object sender, EventArgs e)
        {
            this.Close();
            obj_co.Deconnexion();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Class_utilisateur.Modif_place(textBox1, label_ARA);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Class_utilisateur.Modif_place(textBox2, label_BFC);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Class_utilisateur.Modif_place(textBox3, label_Bre);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Class_utilisateur.Modif_place(textBox4, label_CVL);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Class_utilisateur.Modif_place(textBox5, label_Cor);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Class_utilisateur.Modif_place(textBox6, label_GrE);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Class_utilisateur.Modif_place(textBox7, label_HdF);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Class_utilisateur.Modif_place(textBox8, label_IdF);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Class_utilisateur.Modif_place(textBox9, label_Nor);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Class_utilisateur.Modif_place(textBox10, label_NAq);
        }

        private void button12_Click(object sender, EventArgs e)
        {
            Class_utilisateur.Modif_place(textBox11, label_Occ);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            Class_utilisateur.Modif_place(textBox12, label_PdL);
        }

        private void button13_Click(object sender, EventArgs e)
        {
            Class_utilisateur.Modif_place(textBox13, label_PACA);
        }

        private void button14_Click(object sender, EventArgs e)
        {
            Class_utilisateur.Modif_place(textBox14, label_Gua);
        }

        private void button15_Click(object sender, EventArgs e)
        {
            Class_utilisateur.Modif_place(textBox15, label_Guy);
        }

        private void button16_Click(object sender, EventArgs e)
        {
            Class_utilisateur.Modif_place(textBox16, label_Mar);
        }

        private void button17_Click(object sender, EventArgs e)
        {
            Class_utilisateur.Modif_place(textBox17, label_Reu);
        }

        private void button18_Click(object sender, EventArgs e)
        {
            Class_utilisateur.Modif_place(textBox18, label_May);
        }

        private void btn_accepter_Click(object sender, EventArgs e)
        {
            try
            {
                if(comboBox1.Text == "")
                {
                    MessageBox.Show("Vous devez choisir un utilisateur valide pour pouvoir accepter une demande de voeux.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    string voeu_1 = "UPDATE `voeux` SET `etat` = 'Accepté', `dateIntervention` = '" + DateTime.Now.ToString("yyyy/MM/dd") + "' WHERE `Ordre` = 1 AND `voeux`.`id_utilisateur` = " + obj_co.Select_text("SELECT `id_utilisateur` FROM `utilisateur` WHERE `prenom` = '" + label_v_prenom.Text + "' AND `nom` = '" + label_v_nom.Text + "'") + "";
                    string voeu_2 = "UPDATE `voeux` SET `etat` = 'Classé', `dateIntervention` = '" + DateTime.Now.ToString("yyyy/MM/dd") + "' WHERE `Ordre` = 2 AND `voeux`.`id_utilisateur` = " + obj_co.Select_text("SELECT `id_utilisateur` FROM `utilisateur` WHERE `prenom` = '" + label_v_prenom.Text + "' AND `nom` = '" + label_v_nom.Text + "'") + "";
                    string voeu_3 = "UPDATE `voeux` SET `etat` = 'Classé', `dateIntervention` = '" + DateTime.Now.ToString("yyyy/MM/dd") + "' WHERE `Ordre` = 3 AND `voeux`.`id_utilisateur` = " + obj_co.Select_text("SELECT `id_utilisateur` FROM `utilisateur` WHERE `prenom` = '" + label_v_prenom.Text + "' AND `nom` = '" + label_v_nom.Text + "'") + "";

                    DialogResult rep_utilisateur;
                    rep_utilisateur = MessageBox.Show("Le voeux n°1 en region " + label10.Text + " va être accepté, Les 2 autres voeux seront donc classés.\n\nVoulez vous continuer ?", "Information", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

                    if (rep_utilisateur == DialogResult.OK)
                    {
                        obj_co.Execute_requete(voeu_1);
                        obj_co.Execute_requete(voeu_2);
                        obj_co.Execute_requete(voeu_3);

                        MessageBox.Show("L'utilisateur " + Class_utilisateur.Prenom + " " + Class_utilisateur.Nom + " a bien été affecté a la region " + label10.Text + ".", "Information", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

                        btn_accepter.Enabled = false;
                        btn_accepter_2.Enabled = false;
                        btn_accepter_3.Enabled = false;
                        btn_refuser.Enabled = false;
                        btn_refuser_2.Enabled = false;
                        btn_refuser_3.Enabled = false;
                    }
                }
               
            }
            catch(Exception ex)
            {
                MessageBox.Show("L'erreur : " + ex + " est survenue.","Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void btn_accepter_2_Click(object sender, EventArgs e)
        {
            try
            {
                string test_user = "SELECT `prenom`,`nom` FROM `utilisateur` JOIN `voeux` ON `utilisateur`.`id_utilisateur` = `voeux`.`id_utilisateur` WHERE `etat` = 'Nouveau'";

                if (comboBox1.Text == "")
                {
                    MessageBox.Show("Vous devez choisir un utilisateur valide pour pouvoir accepter une demande de voeux.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    string voeu_1 = "UPDATE `voeux` SET `etat` = 'Classé', `dateIntervention` = '" + DateTime.Now.ToString("yyyy/MM/dd") + "' WHERE `Ordre` = 1 AND `voeux`.`id_utilisateur` = " + obj_co.Select_text("SELECT `id_utilisateur` FROM `utilisateur` WHERE `prenom` = '" + label_v_prenom.Text + "' AND `nom` = '" + label_v_nom.Text + "'") + "";
                    string voeu_2 = "UPDATE `voeux` SET `etat` = 'Accepté', `dateIntervention` = '" + DateTime.Now.ToString("yyyy/MM/dd") + "' WHERE `Ordre` = 2 AND `voeux`.`id_utilisateur` = " + obj_co.Select_text("SELECT `id_utilisateur` FROM `utilisateur` WHERE `prenom` = '" + label_v_prenom.Text + "' AND `nom` = '" + label_v_nom.Text + "'") + "";
                    string voeu_3 = "UPDATE `voeux` SET `etat` = 'Classé', `dateIntervention` = '" + DateTime.Now.ToString("yyyy/MM/dd") + "' WHERE `Ordre` = 3 AND `voeux`.`id_utilisateur` = " + obj_co.Select_text("SELECT `id_utilisateur` FROM `utilisateur` WHERE `prenom` = '" + label_v_prenom.Text + "' AND `nom` = '" + label_v_nom.Text + "'") + "";

                    DialogResult rep_utilisateur;
                    rep_utilisateur = MessageBox.Show("Le voeux n°2 en region " + label1.Text + " va être accepté, Les 2 autres voeux seront donc classés.\n\nVoulez vous continuer ?", "Information", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

                    if (rep_utilisateur == DialogResult.OK)
                    {
                        obj_co.Execute_requete(voeu_1);
                        obj_co.Execute_requete(voeu_2);
                        obj_co.Execute_requete(voeu_3);

                        MessageBox.Show("L'utilisateur " + Class_utilisateur.Prenom + " " + Class_utilisateur.Nom + " a bien été affecté a la region " + label1.Text + ".", "Information", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

                        btn_accepter.Enabled = false;
                        btn_accepter_2.Enabled = false;
                        btn_accepter_3.Enabled = false;
                        btn_refuser.Enabled = false;
                        btn_refuser_2.Enabled = false;
                        btn_refuser_3.Enabled = false;                        

                    }
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("L'erreur : " + ex + " est survenue.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void btn_accepter_3_Click(object sender, EventArgs e)
        {
            try
            {
                if (comboBox1.Text == "")
                {
                    MessageBox.Show("Vous devez choisir un utilisateur valide pour pouvoir accepter une demande de voeux.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    string voeu_1 = "UPDATE `voeux` SET `etat` = 'Classé', `dateIntervention` = '" + DateTime.Now.ToString("yyyy/MM/dd") + "' WHERE `Ordre` = 1 AND `voeux`.`id_utilisateur` = " + obj_co.Select_text("SELECT `id_utilisateur` FROM `utilisateur` WHERE `prenom` = '" + label_v_prenom.Text + "' AND `nom` = '" + label_v_nom.Text + "'") + "";
                    string voeu_2 = "UPDATE `voeux` SET `etat` = 'Classé', `dateIntervention` = '" + DateTime.Now.ToString("yyyy/MM/dd") + "' WHERE `Ordre` = 2 AND `voeux`.`id_utilisateur` = " + obj_co.Select_text("SELECT `id_utilisateur` FROM `utilisateur` WHERE `prenom` = '" + label_v_prenom.Text + "' AND `nom` = '" + label_v_nom.Text + "'") + "";
                    string voeu_3 = "UPDATE `voeux` SET `etat` = 'Accepté', `dateIntervention` = '" + DateTime.Now.ToString("yyyy/MM/dd") + "' WHERE `Ordre` = 3 AND `voeux`.`id_utilisateur` = " + obj_co.Select_text("SELECT `id_utilisateur` FROM `utilisateur` WHERE `prenom` = '" + label_v_prenom.Text + "' AND `nom` = '" + label_v_nom.Text + "'") + "";

                    DialogResult rep_utilisateur;
                    rep_utilisateur = MessageBox.Show("Le voeux n°3 en region " + label12.Text + " va être accepté, Les 2 autres voeux seront donc classés.\n\nVoulez vous continuer ?", "Information", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

                    if (rep_utilisateur == DialogResult.OK)
                    {
                        obj_co.Execute_requete(voeu_1);
                        obj_co.Execute_requete(voeu_2);
                        obj_co.Execute_requete(voeu_3);

                        MessageBox.Show("L'utilisateur " + Class_utilisateur.Prenom + " " + Class_utilisateur.Nom + " a bien été affecté a la region " + label12.Text + ".\n\nIl sera averti par un message dans son espace personnel", "Information", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

                        btn_accepter.Enabled = false;
                        btn_accepter_2.Enabled = false;
                        btn_accepter_3.Enabled = false;
                        btn_refuser.Enabled = false;
                        btn_refuser_2.Enabled = false;
                        btn_refuser_3.Enabled = false;
                    }
                }
               
            }
            catch (Exception ex)
            {
                MessageBox.Show("L'erreur : " + ex + " est survenue.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void btn_refuser_Click(object sender, EventArgs e)
        {
            try
            {
                if (comboBox1.Text == "")
                {
                    MessageBox.Show("Vous devez choisir un utilisateur valide pour pouvoir refuser une demande de voeux.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    string voeu_1 = "UPDATE `voeux` SET `etat` = 'Refusé', `dateIntervention` = '" + DateTime.Now.ToString("yyyy/MM/dd") + "' WHERE `Ordre` = 1 AND `voeux`.`id_utilisateur` = " + obj_co.Select_text("SELECT `id_utilisateur` FROM `utilisateur` WHERE `prenom` = '" + label_v_prenom.Text + "' AND `nom` = '" + label_v_nom.Text + "'") + "";
                    string voeu_2 = "UPDATE `voeux` SET `etat` = 'Refusé', `dateIntervention` = '" + DateTime.Now.ToString("yyyy/MM/dd") + "' WHERE `Ordre` = 2 AND `voeux`.`id_utilisateur` = " + obj_co.Select_text("SELECT `id_utilisateur` FROM `utilisateur` WHERE `prenom` = '" + label_v_prenom.Text + "' AND `nom` = '" + label_v_nom.Text + "'") + "";
                    string voeu_3 = "UPDATE `voeux` SET `etat` = 'Refusé', `dateIntervention` = '" + DateTime.Now.ToString("yyyy/MM/dd") + "' WHERE `Ordre` = 3 AND `voeux`.`id_utilisateur` = " + obj_co.Select_text("SELECT `id_utilisateur` FROM `utilisateur` WHERE `prenom` = '" + label_v_prenom.Text + "' AND `nom` = '" + label_v_nom.Text + "'") + "";

                    DialogResult rep_utilisateur;
                    rep_utilisateur = MessageBox.Show("Le voeux n°1 en region " + label10.Text + " va être refusé.\n\nLe refus d'un voeu entraine le refus de tous les voeux\nLes 2 autres voeux seront donc refusé.\n\nVoulez vous continuer ?", "Information", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

                    if (rep_utilisateur == DialogResult.OK)
                    {
                        obj_co.Execute_requete(voeu_1);
                        obj_co.Execute_requete(voeu_2);
                        obj_co.Execute_requete(voeu_3);

                        MessageBox.Show("Les voeux de l'utilisateur " + Class_utilisateur.Prenom + " " + Class_utilisateur.Nom + " ont été refusés.\n\nIl sera averti par un message dans son espace personnel", "Information", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

                        btn_accepter.Enabled = false;
                        btn_accepter_2.Enabled = false;
                        btn_accepter_3.Enabled = false;
                        btn_refuser.Enabled = false;
                        btn_refuser_2.Enabled = false;
                        btn_refuser_3.Enabled = false;
                    }
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("L'erreur : " + ex + " est survenue.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void btn_refuser_2_Click(object sender, EventArgs e)
        {
            try
            {
                if (comboBox1.Text == "")
                {
                    MessageBox.Show("Vous devez choisir un utilisateur valide pour pouvoir refuser une demande de voeux.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    string voeu_1 = "UPDATE `voeux` SET `etat` = 'Refusé', `dateIntervention` = '" + DateTime.Now.ToString("yyyy/MM/dd") + "' WHERE `Ordre` = 1 AND `voeux`.`id_utilisateur` = " + obj_co.Select_text("SELECT `id_utilisateur` FROM `utilisateur` WHERE `prenom` = '" + label_v_prenom.Text + "' AND `nom` = '" + label_v_nom.Text + "'") + "";
                    string voeu_2 = "UPDATE `voeux` SET `etat` = 'Refusé', `dateIntervention` = '" + DateTime.Now.ToString("yyyy/MM/dd") + "' WHERE `Ordre` = 2 AND `voeux`.`id_utilisateur` = " + obj_co.Select_text("SELECT `id_utilisateur` FROM `utilisateur` WHERE `prenom` = '" + label_v_prenom.Text + "' AND `nom` = '" + label_v_nom.Text + "'") + "";
                    string voeu_3 = "UPDATE `voeux` SET `etat` = 'Refusé', `dateIntervention` = '" + DateTime.Now.ToString("yyyy/MM/dd") + "' WHERE `Ordre` = 3 AND `voeux`.`id_utilisateur` = " + obj_co.Select_text("SELECT `id_utilisateur` FROM `utilisateur` WHERE `prenom` = '" + label_v_prenom.Text + "' AND `nom` = '" + label_v_nom.Text + "'") + "";

                    DialogResult rep_utilisateur;
                    rep_utilisateur = MessageBox.Show("Le voeux n°2 en region " + label10.Text + " va être refusé.\n\nLe refus d'un voeu entraine le refus de tous les voeux\nLes 2 autres voeux seront donc refusé.\n\nVoulez vous continuer ?", "Information", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

                    if (rep_utilisateur == DialogResult.OK)
                    {
                        obj_co.Execute_requete(voeu_1);
                        obj_co.Execute_requete(voeu_2);
                        obj_co.Execute_requete(voeu_3);

                        MessageBox.Show("Les voeux de l'utilisateur " + Class_utilisateur.Prenom + " " + Class_utilisateur.Nom + " ont été refusés.\n\nIl sera averti par un message dans son espace personnel", "Information", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

                        btn_accepter.Enabled = false;
                        btn_accepter_2.Enabled = false;
                        btn_accepter_3.Enabled = false;
                        btn_refuser.Enabled = false;
                        btn_refuser_2.Enabled = false;
                        btn_refuser_3.Enabled = false;
                    }
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("L'erreur : " + ex + " est survenue.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void btn_refuser_3_Click(object sender, EventArgs e)
        {
            try
            {
                if (comboBox1.Text == "")
                {
                    MessageBox.Show("Vous devez choisir un utilisateur valide pour pouvoir refuser une demande de voeux.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    string voeu_1 = "UPDATE `voeux` SET `etat` = 'Refusé', `dateIntervention` = '" + DateTime.Now.ToString("yyyy/MM/dd") + "' WHERE `Ordre` = 1 AND `voeux`.`id_utilisateur` = " + obj_co.Select_text("SELECT `id_utilisateur` FROM `utilisateur` WHERE `prenom` = '" + label_v_prenom.Text + "' AND `nom` = '" + label_v_nom.Text + "'") + "";
                    string voeu_2 = "UPDATE `voeux` SET `etat` = 'Refusé', `dateIntervention` = '" + DateTime.Now.ToString("yyyy/MM/dd") + "' WHERE `Ordre` = 2 AND `voeux`.`id_utilisateur` = " + obj_co.Select_text("SELECT `id_utilisateur` FROM `utilisateur` WHERE `prenom` = '" + label_v_prenom.Text + "' AND `nom` = '" + label_v_nom.Text + "'") + "";
                    string voeu_3 = "UPDATE `voeux` SET `etat` = 'Refusé', `dateIntervention` = '" + DateTime.Now.ToString("yyyy/MM/dd") + "' WHERE `Ordre` = 3 AND `voeux`.`id_utilisateur` = " + obj_co.Select_text("SELECT `id_utilisateur` FROM `utilisateur` WHERE `prenom` = '" + label_v_prenom.Text + "' AND `nom` = '" + label_v_nom.Text + "'") + "";

                    DialogResult rep_utilisateur;
                    rep_utilisateur = MessageBox.Show("Le voeux n°3 en region " + label10.Text + " va être refusé.\n\nLe refus d'un voeu entraine le refus de tous les voeux\nLes 2 autres voeux seront donc refusé.\n\nVoulez vous continuer ?", "Information", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

                    if (rep_utilisateur == DialogResult.OK)
                    {
                        obj_co.Execute_requete(voeu_1);
                        obj_co.Execute_requete(voeu_2);
                        obj_co.Execute_requete(voeu_3);

                        MessageBox.Show("Les voeux de l'utilisateur " + Class_utilisateur.Prenom + " " + Class_utilisateur.Nom + " ont été refusés.\n\nIl sera averti par un message dans son espace personnel", "Information", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

                        btn_accepter.Enabled = false;
                        btn_accepter_2.Enabled = false;
                        btn_accepter_3.Enabled = false;
                        btn_refuser.Enabled = false;
                        btn_refuser_2.Enabled = false;
                        btn_refuser_3.Enabled = false;
                    }
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("L'erreur : " + ex + " est survenue.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBox2.SelectedIndex == 0)
            {                
                comboBox3.Enabled = false;
                dateTimePicker1.Enabled = true;
                comboBox3.Text = null;

            }
            else if(comboBox2.SelectedIndex == 1)
            {
                dateTimePicker1.Enabled = false;
                lbl_filtre.Visible = true;
                lbl_filtre.Text = "Etat";
                comboBox3.Enabled = true;
                comboBox3.Items.Clear();
                comboBox3.Items.Add("Nouveau");
                comboBox3.Items.Add("Accepté");
                comboBox3.Items.Add("Refusé");               
            }
            else if(comboBox2.SelectedIndex == 2)
            {
                dateTimePicker1.Enabled = false;
                lbl_filtre.Visible = true;
                lbl_filtre.Text = "Région";
                comboBox3.Items.Clear();
                comboBox3.Enabled = true;
                comboBox3.Items.Add(label22.Text);

                foreach(Label lbl in tableLayoutPanel1.Controls.OfType<Label>())
                {
                    if(lbl.TabIndex > 17 && lbl.TabIndex < 35)
                    {
                        comboBox3.Items.Add(lbl.Text);
                    }
                }

            }
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(lbl_filtre.Text == "Etat")
            {
                dataGridView1.DataSource = obj_co.DGV_Historique("SELECT `etat`,`utilisateur`.`nom`,`prenom`,`region`.`nom` AS 'region',`dateCreation`,`ordre` FROM `voeux`,`utilisateur`,`region` WHERE `voeux`.`id_utilisateur` = `utilisateur`.`id_utilisateur` AND `voeux`.`id_Region_CHOISIR` = `region`.`id_Region` AND `etat` = '" + comboBox3.SelectedText + "'", "voeux");
            }
            else if(lbl_filtre.Text == "Région")
            {
                dataGridView1.DataSource = obj_co.DGV_Historique("SELECT `region`.`nom` AS 'region',`utilisateur`.`nom`,`prenom`,`dateCreation`,`etat`,`ordre` FROM `voeux`,`utilisateur`,`region` WHERE `voeux`.`id_utilisateur` = `utilisateur`.`id_utilisateur` AND `voeux`.`id_Region_CHOISIR` = `region`.`id_Region` AND `id_Region_CHOISIR` = " + Convert.ToInt32(comboBox3.SelectedIndex + 1) + "", "voeux");
            }

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            dataGridView1.DataSource = obj_co.DGV_Historique("SELECT `dateCreation`,`utilisateur`.`nom`,`prenom`,`region`.`nom` AS 'region',`etat`,`ordre` FROM `voeux`,`utilisateur`,`region` WHERE `voeux`.`id_utilisateur` = `utilisateur`.`id_utilisateur` AND `voeux`.`id_Region_CHOISIR` = `region`.`id_Region` AND `dateCreation` = '" + dateTimePicker1.Value.ToString("yyyy/MM/dd") + "'", "voeux");            
        }
    }
}
