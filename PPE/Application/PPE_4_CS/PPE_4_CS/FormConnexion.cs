﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PPE_4_CS
{
    public partial class FormConnexion : Form
    {
        ClassConnexion obj_co = new ClassConnexion();
        Class_utilisateur user_co = new Class_utilisateur();
        


        public FormConnexion()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Class_utilisateur.Acceptation = 10;
            label13.Text = "En cliquant sur inscription vous acceptez les CGU\net la collecte de vos données.";

        }

        private void btn_connexion_Click(object sender, EventArgs e)
        {
            try
            {
                obj_co.Connexion();

                string val_hash;
                string val_hash2;
                string pass = textBox_c_mdp.Text;
                int essais = 0;

                val_hash = ClassConnexion.EncryptSHA256(pass);
                val_hash2 = ClassConnexion.EncryptSHA256(val_hash);

                string con = "SELECT `login`, `mdp` FROM `utilisateur` WHERE `login` = '" + textBox_c_login.Text + "' AND `mdp` = '" + val_hash2 + "'";
                string statut = obj_co.Select_text("SELECT `id_statut` FROM `utilisateur` WHERE `login` = '" + textBox_c_login.Text + "' AND `mdp` = '" + val_hash2 + "'");

                ChoixVoeux form_2 = new ChoixVoeux();
                AffectationVoeux form_3 = new AffectationVoeux();

                // Connexion de l'utilisateur -------------------------------------------------------------------------------------------------------------------------
                try
                {
                    if (obj_co.Lecture(con) <= 0)
                    {
                        label_message_c.Visible = true;
                        label_message_c.ForeColor = Color.Red;
                        label_message_c.Text = "Mot de passe incorrect. Réessayez";
                        essais = essais + 1;
                    }
                    else
                    {
                        Class_utilisateur.Id = Convert.ToInt32(obj_co.Select_text("SELECT `id_utilisateur` FROM `utilisateur` WHERE `login` = '" + textBox_c_login.Text + "' AND `mdp` = '" + val_hash2 + "'"));
                        Class_utilisateur.Login = textBox_c_login.Text;
                        Class_utilisateur.Prenom = obj_co.Select_text("SELECT `prenom` FROM `utilisateur` WHERE `login` = '" + Class_utilisateur.Login + "'");
                        Class_utilisateur.Nom = obj_co.Select_text("SELECT `nom` FROM `utilisateur` WHERE `login` = '" + Class_utilisateur.Login + "'");

                        string val_dem_voeux = "SELECT `id_utilisateur` FROM `demande_d_un_voeux` WHERE `id_utilisateur` = " + Class_utilisateur.Id + "";
                        string voeu_acc = "SELECT `etat` FROM `voeux` WHERE `id_utilisateur` = " + obj_co.Select_text("SELECT `id_utilisateur` FROM `utilisateur` WHERE `login` = '" + textBox_c_login.Text + "' AND `mdp` = '" + val_hash2 + "'") + " AND `etat` = 'Accepté'";
                        string voeu_ref = "SELECT `etat` FROM `voeux` WHERE `id_utilisateur` = " + obj_co.Select_text("SELECT `id_utilisateur` FROM `utilisateur` WHERE `login` = '" + textBox_c_login.Text + "' AND `mdp` = '" + val_hash2 + "'") + " AND `etat` = 'Refusé'";


                        if (statut == "1" || statut == "3")
                        {
                            if (obj_co.Lecture(val_dem_voeux) <= 0)
                            {
                                Class_utilisateur.Reg_1 = "Non attribué";
                                Class_utilisateur.Reg_2 = "Non attribué";
                                Class_utilisateur.Reg_3 = "Non attribué";
                                Class_utilisateur.Ordre = "Vous n'avez pas de demande de voeux actuelle.\nChoisissez des regions et leur priorité et cliquez sur valider.";
                                Class_utilisateur.Panel = false;
                            }
                            else
                            {
                                if (obj_co.Lecture(voeu_acc) > 0)
                                {
                                    Class_utilisateur.Acceptation = 1;
                                    Class_utilisateur.Reg_1 = obj_co.Select_text("SELECT `nom` FROM `region` JOIN `voeux` ON region.id_Region = voeux.id_Region_CHOISIR  WHERE `id_utilisateur` = " + Class_utilisateur.Id + " AND `Ordre` = 1 ");
                                    Class_utilisateur.Reg_2 = obj_co.Select_text("SELECT `nom` FROM `region` JOIN `voeux` ON region.id_Region = voeux.id_Region_CHOISIR  WHERE `id_utilisateur` = " + Class_utilisateur.Id + " AND `Ordre` = 2 ");
                                    Class_utilisateur.Reg_3 = obj_co.Select_text("SELECT `nom` FROM `region` JOIN `voeux` ON region.id_Region = voeux.id_Region_CHOISIR  WHERE `id_utilisateur` = " + Class_utilisateur.Id + " AND `Ordre` = 3 ");
                                    Class_utilisateur.Panel = true;
                                    Class_utilisateur.Ordre = "Vous avez déjà une demande de voeux actuelle en cours à votre nom.\nVous pouvez la modifier si vous le souhaitez.";
                                }
                                else if (obj_co.Lecture(voeu_ref) > 0)
                                {
                                    Class_utilisateur.Acceptation = 0;
                                    Class_utilisateur.Reg_1 = obj_co.Select_text("SELECT `nom` FROM `region` JOIN `voeux` ON region.id_Region = voeux.id_Region_CHOISIR  WHERE `id_utilisateur` = " + Class_utilisateur.Id + " AND `Ordre` = 1 ");
                                    Class_utilisateur.Reg_2 = obj_co.Select_text("SELECT `nom` FROM `region` JOIN `voeux` ON region.id_Region = voeux.id_Region_CHOISIR  WHERE `id_utilisateur` = " + Class_utilisateur.Id + " AND `Ordre` = 2 ");
                                    Class_utilisateur.Reg_3 = obj_co.Select_text("SELECT `nom` FROM `region` JOIN `voeux` ON region.id_Region = voeux.id_Region_CHOISIR  WHERE `id_utilisateur` = " + Class_utilisateur.Id + " AND `Ordre` = 3 ");
                                    Class_utilisateur.Panel = true;
                                    Class_utilisateur.Ordre = "Vous avez déjà une demande de voeux actuelle en cours à votre nom.\nVous pouvez la modifier si vous le souhaitez.";
                                }
                                else
                                {
                                    Class_utilisateur.Acceptation = -1;
                                    Class_utilisateur.Reg_1 = obj_co.Select_text("SELECT `nom` FROM `region` JOIN `voeux` ON region.id_Region = voeux.id_Region_CHOISIR  WHERE `id_utilisateur` = " + Class_utilisateur.Id + " AND `Ordre` = 1 ");
                                    Class_utilisateur.Reg_2 = obj_co.Select_text("SELECT `nom` FROM `region` JOIN `voeux` ON region.id_Region = voeux.id_Region_CHOISIR  WHERE `id_utilisateur` = " + Class_utilisateur.Id + " AND `Ordre` = 2 ");
                                    Class_utilisateur.Reg_3 = obj_co.Select_text("SELECT `nom` FROM `region` JOIN `voeux` ON region.id_Region = voeux.id_Region_CHOISIR  WHERE `id_utilisateur` = " + Class_utilisateur.Id + " AND `Ordre` = 3 ");
                                    Class_utilisateur.Panel = true;
                                    Class_utilisateur.Ordre = "Vous avez déjà une demande de voeux actuelle en cours à votre nom.\nVous pouvez la modifier si vous le souhaitez.";
                                }


                            }


                            this.Hide();
                            form_2.Show();
                        }
                        else if (statut == "2")
                        {
                            this.Hide();
                            form_3.Show();
                        }

                    }

                    if (essais == 3)
                    {
                        btn_connexion.Enabled = false;
                        label_message_c.ForeColor = Color.Red;
                        label_message_c.Text = "3 essais echoués, veuillez réessayer plus tard";
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Une erreur est survenue : " + ex, "Erreur");
                }
                // Fin de connexion de l'utilisateur -------------------------------------------------------------------------------------------------------------------------

                obj_co.Deconnexion();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Une erreur est survenue : " + ex, "Erreur");
            }
            
        }

        private void btn_inscription_Click(object sender, EventArgs e)
        {
            panel_inscription.Visible = true;
            panel_connexion.Visible = false;           
        }

        private void btn_inscription_i_Click(object sender, EventArgs e)
        {
            try
            {
                obj_co.Connexion();

                string val_hash;
                string val_hash2;
                string pass = textBox_i_mdp.Text;

                val_hash = ClassConnexion.EncryptSHA256(pass);
                val_hash2 = ClassConnexion.EncryptSHA256(val_hash);

                string recherche_login = "SELECT `login` FROM `utilisateur` WHERE `login` = '" + textBox_i_login.Text + "'";
                string inscr = "INSERT INTO `utilisateur` (`nom`, `prenom`, `login`, `mdp`, `date_entree`, `id_statut`) VALUES ('" + textBox_i_nom.Text + "', '" + textBox_i_prenom.Text + "', '" + textBox_i_login.Text + "', '" + val_hash2 + "', '" + textBox_i_annee.Text + "', '" + textBox_i_statut.Text + "'); ";

                try
                {
                    if (textBox_i_login.TextLength < 6)
                    {
                        label_message_i.Visible = true;
                        label_message_i.ForeColor = Color.Red;
                        label_message_i.Text = "Login pas assez de caratères";
                    }
                    else if (textBox_i_mdp.TextLength < 3)
                    {
                        label_message_i.Visible = true;
                        label_message_i.ForeColor = Color.Red;
                        label_message_i.Text = "Mdp pas assez de caratères";
                    }
                    else if (textBox_i_prenom.Text.Length == 0 || textBox_i_nom.Text.Length == 0)
                    {
                        MessageBox.Show("Un ou plusieurs champs n'ont pas été rempli.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else
                    {

                        if (textBox_i_statut.Text == "1" || textBox_i_statut.Text == "2" || textBox_i_statut.Text == "3")
                        {
                            if (textBox_i_annee.Text.Length == 4 && Convert.ToInt32(textBox_i_annee.Text) > 1965 && Convert.ToInt32(textBox_i_annee.Text) < Convert.ToInt32(DateTime.Now.ToString("yyyy")))
                            {
                                if (obj_co.Lecture(recherche_login) <= 0)
                                {
                                    label_message_i.Visible = false;
                                    obj_co.Execute_requete(inscr);

                                    MessageBox.Show("Votre inscription a été validée. Bienvenue.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                    foreach (TextBox tbx in panel_inscription.Controls.OfType<TextBox>())
                                    {
                                        if (tbx.Text != "")
                                        {
                                            tbx.Text = "";
                                        }
                                    }

                                    panel_connexion.Visible = true;
                                    panel_inscription.Visible = false;
                                }
                                else
                                {
                                    label_message_i.Visible = true;
                                    label_message_i.ForeColor = Color.Red;
                                    label_message_i.Text = "Ce login est déjà pris : ";
                                }
                            }
                            else
                            {
                                MessageBox.Show("Le date saisie n'est pas valide.\nLa date saisie doit être comprise entre 1965 et la date actuelle.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Le statut saisit n'est pas valide.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }


                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Une erreur est survenue : " + ex, "Erreur");
                }



                obj_co.Deconnexion();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Une erreur est survenue : " + ex, "Erreur");
            }
            
        }

        private void btn_annuler_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (TextBox tbx in panel_inscription.Controls.OfType<TextBox>())
                {
                    if (tbx.Text != "")
                    {
                        tbx.Text = "";
                    }
                }

                label_message_i.Visible = false;

                panel_connexion.Visible = true;
                panel_inscription.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Une erreur est survenue : " + ex, "Erreur");
            }
            
        }

        private void textBox_i_annee_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (Char.IsControl(e.KeyChar) || !Char.IsNumber(e.KeyChar))
                {
                    e.Handled = true; // Set l'evenement comme etant completement fini                
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Une erreur est survenue : " + ex, "Erreur");
            }
               

        }

        private void textBox_i_annee_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyValue == (char)Keys.Back)
                {

                    ((TextBox)sender).Text = ((TextBox)sender).Text.Substring(0, ((TextBox)sender).Text.Length - 1);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Une erreur est survenue : " + ex, "Erreur");
            }
            
        }

        private void label13_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Suite au RGPD, en vous inscrivant vous consentez à la collecte de vos données personnelles" +
                " à savoir Nom et Prénom dans notre base de données.\nVous possédez différents droits que vous pouvez exercer à tous moment :" +
                "\n- Droit à l'oubli\n- Droit à l'information\n- Droit à la portabilité des données\n- Droit à la réctification"
                , "CGU et RGPD",MessageBoxButtons.OK,MessageBoxIcon.Information);
        }
    }
}
