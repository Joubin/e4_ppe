﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
using System.Security.Cryptography;
using System.Windows.Forms;

namespace PPE_4_CS
{
    class ClassConnexion
    {

        string chaine_connexion = "SERVER = 127.0.0.1; DATABASE = ppe_4; UID = root; PASSWORD = ''";


        MySqlConnection ma_connexion;



        public void Connexion()
        {
            ma_connexion = new MySqlConnection(chaine_connexion);
            ma_connexion.Open();
        }

        public void Deconnexion()
        {
            ma_connexion.Close();
        }

        public void Execute_requete(string ma_requete)
        {
            MySqlCommand ma_commande = new MySqlCommand(ma_requete, ma_connexion);
            ma_commande.ExecuteNonQuery();
        }

        public string Execute_requete_text(string ma_requete)
        {

            string resultat;

            MySqlCommand ma_commande = new MySqlCommand(ma_requete, ma_connexion);
            resultat = Convert.ToString(ma_commande.ExecuteNonQuery());

            return resultat;
        }

        public string Select_text(string ma_requete)
        {
            string resultat;
            MySqlCommand ma_commande = new MySqlCommand();
            try
            {
                ma_commande.Connection = ma_connexion;
                ma_commande.CommandType = CommandType.Text;
                ma_commande.CommandText = ma_requete;               
            }
            catch (Exception ex)
            {
                MessageBox.Show("Une erreur est survenue : " + ex, "Erreur");
            }
            resultat = Convert.ToString(ma_commande.ExecuteScalar());
            return resultat;
        }


        public MySqlDataReader Lecture_Donnees(string ma_requete)
        {
            MySqlCommand ma_commande = new MySqlCommand(ma_requete, ma_connexion);
            MySqlDataReader mon_reader = ma_commande.ExecuteReader();
           
            return mon_reader;

        }

        public int Lecture(string ma_requete)
        {
           
            int compte;

            MySqlDataAdapter mon_adapter = new MySqlDataAdapter(ma_requete, chaine_connexion);
            DataTable ma_table = new DataTable();
            mon_adapter.Fill(ma_table);
            compte = ma_table.Rows.Count;
            return compte;

        }        

        public MySqlDataReader combo()
        {

            MySqlCommand ma_c = new MySqlCommand("SELECT `nom` FROM `utilisateur` WHERE `id_statut` = 3", ma_connexion);
            MySqlDataReader mon_re;
            mon_re = ma_c.ExecuteReader();

            return mon_re;

        }

        public MySqlDataReader combo_general(string requete)
        {

            MySqlCommand ma_c = new MySqlCommand(requete, ma_connexion);
            MySqlDataReader mon_re;
            mon_re = ma_c.ExecuteReader();

            return mon_re;

        }

        public DataTable DGV_Historique(string requete, string table)
        {

            MySqlCommand ma_com = new MySqlCommand(requete, ma_connexion);
            MySqlDataAdapter mon_da = new MySqlDataAdapter(ma_com);
            DataSet mon_ds = new DataSet();
            DataTable mon_dt = new DataTable();
            mon_da.Fill(mon_ds, table);
            mon_dt = mon_ds.Tables[table];

            return mon_dt;
        }

        public static string EncryptSHA256(string pass)
        {
            using (SHA256CryptoServiceProvider sha256 = new SHA256CryptoServiceProvider())
            {
                string val_hash;
                UTF8Encoding utf = new UTF8Encoding();
                byte[] data = sha256.ComputeHash(utf.GetBytes(pass));
                val_hash = Convert.ToBase64String(data);
                return val_hash;

            }
        }

    }
}
