-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 17 mai 2019 à 14:40
-- Version du serveur :  5.7.21
-- Version de PHP :  5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ppe_4`
--

-- --------------------------------------------------------

--
-- Structure de la table `demande_d_un_voeux`
--

DROP TABLE IF EXISTS `demande_d_un_voeux`;
CREATE TABLE IF NOT EXISTS `demande_d_un_voeux` (
  `id_utilisateur` int(11) NOT NULL,
  `id_Voeux` int(11) NOT NULL,
  PRIMARY KEY (`id_utilisateur`,`id_Voeux`),
  KEY `Demande_D_un_voeux_VOEUX0_FK` (`id_Voeux`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `demande_d_un_voeux`
--

INSERT INTO `demande_d_un_voeux` (`id_utilisateur`, `id_Voeux`) VALUES
(1, 46),
(1, 47),
(1, 48);

-- --------------------------------------------------------

--
-- Structure de la table `intervenir`
--

DROP TABLE IF EXISTS `intervenir`;
CREATE TABLE IF NOT EXISTS `intervenir` (
  `id_Voeux` int(11) NOT NULL,
  `id_utilisateur` int(11) NOT NULL,
  PRIMARY KEY (`id_Voeux`,`id_utilisateur`),
  KEY `Intervenir_Utilisateur0_FK` (`id_utilisateur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `region`
--

DROP TABLE IF EXISTS `region`;
CREATE TABLE IF NOT EXISTS `region` (
  `id_Region` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  `nbPlace` int(11) NOT NULL,
  PRIMARY KEY (`id_Region`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `region`
--

INSERT INTO `region` (`id_Region`, `nom`, `nbPlace`) VALUES
(1, 'Auvergne-Rhône-Alpes', 3),
(2, 'Bourgogne-Franche-Comté', 5),
(3, 'Bretagne', 2),
(4, 'Centre-Val de Loire', 6),
(5, 'Corse', 1),
(6, 'Grand Est', 3),
(7, 'Hauts-de-France', 2),
(8, 'Île-de-France', 0),
(9, 'Normandie', 0),
(10, 'Nouvelle-Aquitaine', 5),
(11, 'Occitanie', 4),
(12, 'Pays de la Loire', 2),
(13, 'Provence-Alpes-Côte d\'Azur', 3),
(14, 'Guadeloupe', 2),
(15, 'Guyane', 1),
(16, 'Martinique', 3),
(17, 'La Réunion', 0),
(18, 'Mayotte', 1);

-- --------------------------------------------------------

--
-- Structure de la table `statut_utilisateur`
--

DROP TABLE IF EXISTS `statut_utilisateur`;
CREATE TABLE IF NOT EXISTS `statut_utilisateur` (
  `id_statut` int(11) NOT NULL AUTO_INCREMENT,
  `nom_statut` varchar(50) NOT NULL,
  PRIMARY KEY (`id_statut`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `statut_utilisateur`
--

INSERT INTO `statut_utilisateur` (`id_statut`, `nom_statut`) VALUES
(1, 'visiteur'),
(2, 'ressources humaines'),
(3, 'administrateur');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id_utilisateur` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `login` varchar(50) NOT NULL,
  `mdp` varchar(50) NOT NULL,
  `date_entree` year(4) NOT NULL,
  `id_statut` int(11) NOT NULL,
  PRIMARY KEY (`id_utilisateur`),
  KEY `Utilisateur_Statut_utilisateur_FK` (`id_statut`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id_utilisateur`, `nom`, `prenom`, `login`, `mdp`, `date_entree`, `id_statut`) VALUES
(1, 'test_nom', 'test_prenom', 'test_login', 'pChCPkINrdGmxJkVzefPSwX9J4iqwQziFdp8OU9UrfM=', 1996, 3),
(2, 'test_nom_2', 'test_prenom_2', 'test_login_2', 'krLjzBln8cRMI9HNjC32hBtuf7qO31ETbdpvRyN6JHE=', 2005, 3),
(3, 'test_nom_3', 'test_prenom_3', 'test_login_3', 'aOi2gySVkqBr3ganMvimva9ACZYEBRVPTWIzVsBkixw=', 2000, 1),
(4, 'test_nom_4', 'test_prenom_4', 'test_login_4', 'xb4I5AexrNRlD8BSc2AhUtv4D7b4VklIBtbI8KakKqk=', 2001, 1),
(5, '', '', 'test_login_5', 'IbZEvp/DP9CdkXnNWntZyHWiF8/gNNKQ1GvckRZI5YI=', 2009, 1),
(6, 'rhn', 'rhp', 'loginrh', 'S4eJmPYy/sI52rMpRTsI1AbEXuE046gu9iPwCVjMiDo=', 2001, 2);

-- --------------------------------------------------------

--
-- Structure de la table `voeux`
--

DROP TABLE IF EXISTS `voeux`;
CREATE TABLE IF NOT EXISTS `voeux` (
  `id_Voeux` int(11) NOT NULL AUTO_INCREMENT,
  `id_utilisateur` int(11) NOT NULL,
  `id_Region` int(11) DEFAULT NULL,
  `Ordre` int(11) NOT NULL,
  `etat` varchar(50) NOT NULL,
  `DateCreation` date NOT NULL,
  `DateIntervention` date DEFAULT NULL,
  `id_Region_CHOISIR` int(11) NOT NULL,
  PRIMARY KEY (`id_Voeux`),
  KEY `VOEUX_Region_FK` (`id_Region_CHOISIR`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `voeux`
--

INSERT INTO `voeux` (`id_Voeux`, `id_utilisateur`, `id_Region`, `Ordre`, `etat`, `DateCreation`, `DateIntervention`, `id_Region_CHOISIR`) VALUES
(46, 1, NULL, 1, '', '2019-03-01', NULL, 4),
(47, 1, NULL, 2, '', '2019-03-01', NULL, 5),
(48, 1, NULL, 3, '', '2019-03-01', NULL, 6);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `demande_d_un_voeux`
--
ALTER TABLE `demande_d_un_voeux`
  ADD CONSTRAINT `Demande_D_un_voeux_Utilisateur_FK` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id_utilisateur`),
  ADD CONSTRAINT `Demande_D_un_voeux_VOEUX0_FK` FOREIGN KEY (`id_Voeux`) REFERENCES `voeux` (`id_Voeux`);

--
-- Contraintes pour la table `intervenir`
--
ALTER TABLE `intervenir`
  ADD CONSTRAINT `Intervenir_Utilisateur0_FK` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id_utilisateur`),
  ADD CONSTRAINT `Intervenir_VOEUX_FK` FOREIGN KEY (`id_Voeux`) REFERENCES `voeux` (`id_Voeux`);

--
-- Contraintes pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD CONSTRAINT `Utilisateur_Statut_utilisateur_FK` FOREIGN KEY (`id_statut`) REFERENCES `statut_utilisateur` (`id_statut`);

--
-- Contraintes pour la table `voeux`
--
ALTER TABLE `voeux`
  ADD CONSTRAINT `VOEUX_Region_FK` FOREIGN KEY (`id_Region_CHOISIR`) REFERENCES `region` (`id_Region`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
